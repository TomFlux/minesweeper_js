let cells = [];

let hiderCells;
let flagCells;

let bombs = [];

let rows, cols, numBombs;
const canvasSize = 1000;

let numFlags = 0;

let playing = true;

let finish_text = "";

const debug = false;

const checkCells = [
  [1, -1],
  [1, 0],
  [1, 1],
  [0, -1],
  [0, 1],
  [-1, -1],
  [-1, 0],
  [-1, 1]
];

class Cell {
  constructor(x, y) {
    this.x = x;
    this.y = y;
    this.center = [x + sqrSize / 2, y + sqrSize / 2];
    this.touching = 0;

    this.display = () => {
      if (this.bomb) {
        fill("red");
        square(this.x, this.y, sqrSize);
      } else {
        fill("white");
        square(this.x, this.y, sqrSize);

        textSize(sqrSize / 4);
        fill("black");

        if (this.touching > 0)
          text(this.touching, this.center[0], this.center[1]);
      }
    };

    this.updateNeighbours = () => {
      checkCells.forEach(yx => {
        let lookY = Math.floor(this.y / sqrSize) + yx[0];
        let lookX = Math.floor(this.x / sqrSize) + yx[1];

        if (
          lookY > -1 &&
          lookY < cells.length &&
          lookX > -1 &&
          lookX < cells[0].length
        ) {
          cells[lookY][lookX].touching += 1;
        }
      });
    };
  }
}

function checkZeros(x, y) {
  let shown = [[x, y]];

  while (shown.length > 0) {
    if (cells[shown[0][1]][shown[0][0]].touching == 0) {
      checkCells.forEach(yx => {
        let Y = shown[0][1] + yx[0];
        let X = shown[0][0] + yx[1];

        if (
          Y > -1 &&
          Y < cells.length &&
          X > -1 &&
          X < cells[0].length &&
          flagCells[Y][X] != true
        ) {
          if (hiderCells[Y][X] == true) {
            hiderCells[Y][X] = false;

            if (cells[Y][X].touching == 0) shown.push([X, Y]);
          }
        }
      });
    }

    shown.shift(1);
  }
}

function setup() {
  rows = document.getElementById("rows").value;
  cols = document.getElementById("cols").value;
  numBombs = document.getElementById("bombs").value;

  let sqrSize = canvasSize / cols;

  createCanvas(canvasSize, canvasSize);

  playing = true;

  cells = [];
  flagCells = [];
  hiderCells = [];
  bombs = [];

  numFlags = 0;

  // disable right click
  document
    .getElementById("defaultCanvas0")
    .addEventListener("contextmenu", e => e.preventDefault());

  document.getElementById("status").innerHTML = "";

  // push bombs onto array until we have enough (1D)
  for (i = 0; i < cols * rows; i++) {
    if (bombs.length < numBombs) {
      bombs.push(true);
    } else {
      bombs.push(false);
    }
  }

  _bombs = shuffle(bombs);
  bombs = [];

  // convert into 2D array
  while (_bombs.length) bombs.push(_bombs.splice(0, rows));

  const arr_create = (len, v) => {
    let a = [];
    for (i = 0; i < len; i++) {
      a.push(v);
    }
    return a;
  };

  hiderCells = arr_create(cols, true).map(x => arr_create(rows, x));
  flagCells = arr_create(cols, false).map(x => arr_create(rows, x));

  // create board
  for (y = 0; y < cols; y++) {
    let row = [];
    let rowH = [];
    let rowF = [];

    for (x = 0; x < rows; x++) {
      let c = new Cell(x * sqrSize, y * sqrSize);

      c.bomb = bombs[y][x];

      c.display();

      row.push(c);

      // rowH.push(true);
      // rowF.push(false);
    }
    cells.push(row);

    // hiderCells.push(rowH);
    // flagCells.push(rowF);
  }

  cells.forEach(row => {
    row.forEach(cell => {
      if (cell.bomb) {
        cell.updateNeighbours();
      }
    });
  });

  for (y = 0; y < cols; y++) {
    for (x = 0; x < rows; x++) {
      if (flagCells[y][x]) {
        fill("blue");
        square(x * sqrSize, y * sqrSize, sqrSize);
      } else if (hiderCells[y][x]) {
        fill("green");
        square(x * sqrSize, y * sqrSize, sqrSize);
      } else {
        cells[y][x].display();
      }
    }
  }
}

function draw() {
  document.getElementById("numBombs").innerHTML = `Bombs: ${numBombs}`;
  document.getElementById("numFlags").innerHTML = `Flags: ${numFlags}`;

  document.getElementById("numRows").innerHTML = `Rows: ${
    document.getElementById("rows").value
  }`;
  document.getElementById("numCols").innerHTML = `Cols: ${
    document.getElementById("cols").value
  }`;

  if (debug)
    document.getElementById("mousepos").innerHTML = `x:${mouseX}\ty:${mouseY}`;

  background(255);

  if (!playing) {
    document.getElementById("status").innerHTML = finish_text;
    // document.getElementById("restart_button").style.visibility = visible;
  }

  for (y = 0; y < cols; y++) {
    for (x = 0; x < rows; x++) {
      if (flagCells[y][x]) {
        fill("blue");
        square(x * sqrSize, y * sqrSize, sqrSize);
      } else if (hiderCells[y][x]) {
        fill("green");
        square(x * sqrSize, y * sqrSize, sqrSize);
      } else {
        cells[y][x].display();
      }
    }
  }

  let same = true;

  for (y = 0; y < cols; y++) {
    for (x = 0; x < rows; x++) {
      if (bombs[y][x] == !hiderCells[y][x] || bombs[y][x] == !flagCells[y][x]) {
        same = false;
      }
    }
  }

  if (same) {
    playing = false;
    finish_text = "Win";
  }
}

function mousePressed() {
  // console.log(`mouseX: ${mouseX}\tmouseY: ${mouseY}`);

  if (
    playing &&
    mouseX > 0 &&
    mouseX < rows * sqrSize &&
    mouseY > 0 &&
    mouseY < cols * sqrSize
  ) {
    let mouseYX = [Math.floor(mouseY / sqrSize), Math.floor(mouseX / sqrSize)];

    if (mouseButton == LEFT) {
      const clickedCell = cells[mouseYX[0]][mouseYX[1]];
      const clickedHCell = hiderCells[mouseYX[0]][mouseYX[1]];

      if (!flagCells[mouseYX[0]][mouseYX[1]]) {
        if (clickedCell.bomb) {
          playing = false;
          finish_text = "Lose";

          for (y = 0; y < cols; y++) {
            for (x = 0; x < rows; x++) {
              hiderCells[y][x] = false;
            }
          }
        } else if (clickedHCell) {
          hiderCells[mouseYX[0]][mouseYX[1]] = false;

          if (clickedCell.touching == 0) {
            checkZeros(mouseYX[1], mouseYX[0]);
          }
        }
      }
    } else if (mouseButton == RIGHT && hiderCells[mouseYX[0]][mouseYX[1]]) {
      if (flagCells[mouseYX[0]][mouseYX[1]]) {
        flagCells[mouseYX[0]][mouseYX[1]] = false;
        numFlags--;
      } else {
        flagCells[mouseYX[0]][mouseYX[1]] = true;
        numFlags++;
      }
    }
  }
}
